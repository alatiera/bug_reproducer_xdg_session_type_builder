pub static PKGDATADIR: &'static str = "/app/share/envreproducer";
pub static VERSION: &'static str = "0.1.0";
pub static LOCALEDIR: &'static str = "/app/share/locale";