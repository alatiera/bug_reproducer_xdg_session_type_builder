use gtk::prelude::*;
use gio::prelude::*;
use gettextrs::*;
use std::env;

mod config;
mod window;
use crate::window::Window;

use std::io::Write;
use std::process::Command;
use x11::xlib;

fn main() {
    if let Ok(var) = env::var("XDG_SESSION_TYPE") {
        if var == "x11" {
            unsafe { xlib::XInitThreads() };
            panic!("I RUN");
        }
    };

    println!("Env dump from cli:");
    let output = Command::new("sh")
            .arg("-c")
            .arg("env")
            .output()
            .expect("failed to execute process");

    std::io::stdout().write_all(&output.stdout).unwrap();
    std::io::stderr().write_all(&output.stderr).unwrap();
    assert!(output.status.success());
    panic!(dbg!(env::var("XDG_SESSION_TYPE")));

    gtk::init().unwrap_or_else(|_| panic!("Failed to initialize GTK."));

    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain("envreproducer", config::LOCALEDIR);
    textdomain("envreproducer");

    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/envreproducer.gresource")
                                .expect("Could not load resources");
    gio::resources_register(&res);

    let app = gtk::Application::new(Some("org.example.App"), Default::default()).unwrap();
    app.connect_activate(move |app| {
        let window = Window::new();

        window.widget.set_application(Some(app));
        app.add_window(&window.widget);
        window.widget.present();
    });


    let args: Vec<String> = env::args().collect();
    app.run(&args);

}

